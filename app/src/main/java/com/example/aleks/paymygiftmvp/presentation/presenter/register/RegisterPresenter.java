package com.example.aleks.paymygiftmvp.presentation.presenter.register;


import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.example.aleks.paymygiftmvp.PMGApplication;
import com.example.aleks.paymygiftmvp.R;
import com.example.aleks.paymygiftmvp.commons.Utils;
import com.example.aleks.paymygiftmvp.model.RegisterUser;
import com.example.aleks.paymygiftmvp.presentation.view.register.RegisterView;
import com.example.aleks.paymygiftmvp.ui.activity.main.Screens;

import java.io.File;
import java.util.Date;

import javax.inject.Inject;

import ru.terrakok.cicerone.Router;

@InjectViewState
public class RegisterPresenter extends MvpPresenter<RegisterView> {

    private static final String TAG = "RegisterPresenter";
    @Inject
    Router router;
    @Inject
    RegisterUser userModel;
    private boolean isButtonClicked = false;
    private File currentPhotoFile;

    public RegisterPresenter() {
        PMGApplication.getComponent().inject(this);
    }

    public File getCurrentPhotoFile() {
        return currentPhotoFile;
    }

    public void setCurrentPhotoFile(File currentPhotoFile) {
        this.currentPhotoFile = currentPhotoFile;
    }

    public void bindValues() {
        if (userModel.getAvatarURL() != null) {
            getViewState().setAvatar(userModel.getAvatarURL());
        } else if (userModel.getSmallAvatarURL() != null) {
            getViewState().setAvatar(userModel.getSmallAvatarURL());
        }
        if (userModel.getNickName() != null) {
            getViewState().setName(userModel.getNickName());
        }
        if (userModel.getLogin() != null) {
            getViewState().setNickName(userModel.getLogin());
        }
        if (userModel.getCity() != null) {
            getViewState().setCity(userModel.getCity());
        }
        getViewState().setGender(userModel.isGender());
        if (userModel.getDate() != null) {
            getViewState().setBirthday(userModel.getDate());
        }
    }

    public void goBack() {
        router.exit();
    }

    public void goToTelephone() {
        isButtonClicked = true;
        if (validate())
            router.navigateTo(Screens.TELEPHONE_SCREEN);
    }

    private boolean validate() {
        return validateName() & validateNickName() & validateCity() & validateBirthday();
    }

    private boolean validateName() {
        if (userModel.getNickName() != null && !userModel.getNickName().equals("")) {
            getViewState().setNameError(null);
            return true;
        }
        getViewState().setNameError(R.string.register_necessary_field);
        return false;
    }

    private boolean validateNickName() {
        if (userModel.getLogin() != null && !userModel.getLogin().equals("")) {
            getViewState().setNickNameError(null);
            return true;
        }
        getViewState().setNickNameError(R.string.register_necessary_field);
        return false;
    }

    private boolean validateCity() {
        if (userModel.getCity() != null && !userModel.getCity().equals("")) {
            getViewState().setCityError(null);
            return true;
        }
        getViewState().setCityError(R.string.register_necessary_field);
        return false;
    }

    private boolean validateBirthday() {
        if (userModel.getDate() != null && !userModel.getDate().equals("")) {
            getViewState().setBirthdayError(null);
            return true;
        }
        getViewState().setBirthdayError(R.string.register_error_birthday);
        return false;
    }

    public void setDate(long date) {
        userModel.setDate(Utils.convertFromDateYYYYMMDD(new Date(date)));
        getViewState().setBirthday(userModel.getDate());
        if (isButtonClicked)
            validateBirthday();
    }

    public void setName(String name) {
        userModel.setNickName(name);
        if (isButtonClicked)
            validateName();
    }

    public void setNickName(String nickName) {
        userModel.setLogin(nickName);
        if (isButtonClicked)
            validateNickName();
    }

    public void setCity(String city) {
        userModel.setCity(city);
        if (isButtonClicked)
            validateCity();
    }

    public void setGender(boolean gender) {
        userModel.setGender(gender);
        getViewState().setGender(gender);
    }

    public void setAvatarFromFile() {
        userModel.setAvatarURL(null);
        userModel.setSmallAvatarURL(currentPhotoFile.toString());
        getViewState().setAvatar(userModel.getSmallAvatarURL());
    }
}
