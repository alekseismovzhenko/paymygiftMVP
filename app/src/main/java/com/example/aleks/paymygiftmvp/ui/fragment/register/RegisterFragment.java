package com.example.aleks.paymygiftmvp.ui.fragment.register;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.example.aleks.paymygiftmvp.R;
import com.example.aleks.paymygiftmvp.commons.DatePickerFragment;
import com.example.aleks.paymygiftmvp.commons.Utils;
import com.example.aleks.paymygiftmvp.model.ResponseCities;
import com.example.aleks.paymygiftmvp.presentation.presenter.register.RegisterPresenter;
import com.example.aleks.paymygiftmvp.presentation.view.register.RegisterView;
import com.jakewharton.rxbinding2.widget.RxTextView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class RegisterFragment extends MvpAppCompatFragment implements RegisterView {

    public static final String TAG = "RegisterFragment";
    public static final String CITIES_TAG = "cities";
    private static final int REQUEST_DATE = 11;
    private static final int REQUEST_PERMISSIONS = 100;
    private static final int TAKE_PHOTO_ACTION = 111;
    private static final int PICK_PHOTO_ACTION = 222;
    @BindView(R.id.registerAvatarImageView)
    CircleImageView avatarImageView;
    @BindView(R.id.registerLoadPhotoTextView)
    TextView loadPhotoTextView;
    @BindView(R.id.registerNameTextInputLayout)
    TextInputLayout nameTextInputLayout;
    @BindView(R.id.registerNicknameTextInputLayout)
    TextInputLayout nickNameTextInputLayout;
    @BindView(R.id.registerCityInputLayout)
    TextInputLayout cityTextInputLayout;
    @BindView(R.id.registerGenderInputLayout)
    TextInputLayout genderTextInputLayout;
    @BindView(R.id.registerBirthDateInputLayout)
    TextInputLayout birthdayTextInputLayout;
    @InjectPresenter
    RegisterPresenter mRegisterPresenter;
    private List<String> cities = new ArrayList<>();

    public static RegisterFragment newInstance(ResponseCities responseCities) {
        RegisterFragment fragment = new RegisterFragment();
        Bundle args = new Bundle();
        args.putSerializable(CITIES_TAG, responseCities);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            cities.addAll(((ResponseCities) getArguments().getSerializable(CITIES_TAG)).getCitys());
        }
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             final Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_register, container, false);
        ButterKnife.bind(this, view);
        mRegisterPresenter.bindValues();
        Utils.setupUI(view, getActivity());
        initializeWatchers();
        ((AutoCompleteTextView) cityTextInputLayout.getEditText())
                .setAdapter(new ArrayAdapter<>(getContext(), android.R.layout.simple_dropdown_item_1line, cities));
        return view;
    }

    private void initializeWatchers() {
        RxTextView.textChanges(nameTextInputLayout.getEditText())
                .debounce(300, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(charSequence -> mRegisterPresenter.setName(charSequence.toString()));
        RxTextView.textChanges(nickNameTextInputLayout.getEditText())
                .debounce(300, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(charSequence -> mRegisterPresenter.setNickName(charSequence.toString()));
        RxTextView.textChanges(cityTextInputLayout.getEditText())
                .debounce(300, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(charSequence -> mRegisterPresenter.setCity(charSequence.toString()));
    }

    @OnClick(R.id.registerBackButton)
    public void goBack() {
        mRegisterPresenter.goBack();
    }

    @Override
    public void setAvatar(Object object) {
        Glide
                .with(this)
                .load(object)
                .into(avatarImageView);
        loadPhotoTextView.setText(R.string.register_change_photo);
    }

    @Override
    public void setName(String string) {
        nameTextInputLayout.getEditText().setText(string);
    }

    @Override
    public void setNickName(String string) {
        nickNameTextInputLayout.getEditText().setText(string);
    }

    @Override
    public void setCity(String string) {
        cityTextInputLayout.getEditText().setText(string);
    }

    @Override
    public void setGender(boolean gender) {
        if (gender)
            genderTextInputLayout
                    .getEditText()
                    .setText(getResources().getStringArray(R.array.register_genders)[1]);
        else
            genderTextInputLayout
                    .getEditText()
                    .setText(getResources().getStringArray(R.array.register_genders)[0]);
    }

    @Override
    public void setBirthday(String string) {
        birthdayTextInputLayout
                .getEditText()
                .setText(Utils.convertFromDateDDMMMMYYYY(Utils.convertToDateYYYYMMDD(string)));
    }

    @Override
    public void setBirthdayError(Integer error) {
        if (error != null)
            birthdayTextInputLayout.setError(getString(error));
        else
            birthdayTextInputLayout.setError(null);
    }

    @Override
    public void setNameError(Integer error) {
        if (error != null)
            nameTextInputLayout.setError(getString(error));
        else
            nameTextInputLayout.setError(null);
    }

    @Override
    public void setNickNameError(Integer error) {
        if (error != null)
            nickNameTextInputLayout.setError(getString(error));
        else
            nickNameTextInputLayout.setError(null);
    }

    @Override
    public void setCityError(Integer error) {
        if (error != null)
            cityTextInputLayout.setError(getString(error));
        else
            cityTextInputLayout.setError(null);
    }

    @OnClick(R.id.registerContinueButton)
    public void goToTelephone() {
        mRegisterPresenter.goToTelephone();
    }

    @OnClick({R.id.registerBirthDateInputLayout, R.id.registerBirthDateEditText})
    public void showDateDialog() {
        DatePickerFragment datePickerFragment = new DatePickerFragment();
        datePickerFragment.setTargetFragment(this, REQUEST_DATE);
        datePickerFragment.show(getFragmentManager(), "datepicker");
    }

    @OnClick({R.id.registerGenderInputLayout, R.id.registerGenderEditText})
    public void showGenderDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(R.string.register_choose_gender);
        builder.setSingleChoiceItems(R.array.register_genders, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == 0) {
                    mRegisterPresenter.setGender(false);
                } else {
                    mRegisterPresenter.setGender(true);
                }
                dialog.dismiss();
            }
        });
        builder.create().show();
    }

    @OnClick(R.id.registerLoadPhotoTextView)
    public void choosePhoto() {
        if (checkPermissions()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setTitle(R.string.dialog_photo_choose_action);
            String[] strings = getResources().getStringArray(R.array.dialog_photo_options);
            builder.setItems(strings, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (which == 0) {
                        takeAPhoto();
                    } else {
                        pickAPhoto();
                    }
                }
            });
            builder.create().show();
        } else {
            askForPermissions();
        }
    }

    private void askForPermissions() {
        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                REQUEST_PERMISSIONS);
    }

    private void pickAPhoto() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_PHOTO_ACTION);
    }

    private void takeAPhoto() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            // Create the File where the photo should go
            createImageFile();
            // Continue only if the File was successfully created
            if (mRegisterPresenter.getCurrentPhotoFile() != null) {
                Uri photoURI = FileProvider.getUriForFile(getContext(),
                        "com.example.aleks.paymygiftmvp.provider",
                        mRegisterPresenter.getCurrentPhotoFile());
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, TAKE_PHOTO_ACTION);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_PERMISSIONS: {
                for (int i : grantResults) {
                    if (i != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                }
                choosePhoto();
            }
        }
    }

    private void createImageFile() {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        try {
            if (mRegisterPresenter.getCurrentPhotoFile() != null && mRegisterPresenter.getCurrentPhotoFile().exists()) {
                mRegisterPresenter.getCurrentPhotoFile().delete();
            }
            mRegisterPresenter.setCurrentPhotoFile(File.createTempFile(
                    imageFileName,  /* prefix */
                    ".jpg",         /* suffix */
                    storageDir      /* directory */
            ));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case REQUEST_DATE: {
                    mRegisterPresenter.setDate(data.getLongExtra("date", -1));
                    break;
                }
                case TAKE_PHOTO_ACTION: {
                    mRegisterPresenter.setAvatarFromFile();
                    break;
                }
                case PICK_PHOTO_ACTION: {
                    if (data != null && data.getData() != null) {
                        createImageFile();
                        Glide
                                .with(getContext())
                                .asBitmap()
                                .load(data.getData())
                                .into(new SimpleTarget<Bitmap>() {
                                    @Override
                                    public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                                        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
                                        resource.compress(Bitmap.CompressFormat.JPEG, 100, outStream);
                                        FileOutputStream fileOutputStream = null;
                                        try {
                                            fileOutputStream = new FileOutputStream(mRegisterPresenter.getCurrentPhotoFile());
                                            fileOutputStream.write(outStream.toByteArray());
                                            fileOutputStream.close();
                                            mRegisterPresenter.setAvatarFromFile();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });
                    }
                    break;
                }
            }
        }
    }

    private boolean checkPermissions() {
        return (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
                &&
                (ContextCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
    }
}
