package com.example.aleks.paymygiftmvp.dagger.module;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.terrakok.cicerone.Cicerone;
import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.Router;

/**
 * Created by aleks on 24.06.2017.
 */

@Module
public class NavigatorModule {
    private Cicerone<Router> cicerone;


    public NavigatorModule() {
        cicerone = Cicerone.create();
    }

    @Provides
    @Singleton
    Router provideRouter() {
        return cicerone.getRouter();
    }

    @Provides
    @Singleton
    NavigatorHolder provideNavigationHolder() {
        return cicerone.getNavigatorHolder();
    }
}
