package com.example.aleks.paymygiftmvp.ui.activity.main;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.example.aleks.paymygiftmvp.PMGApplication;
import com.example.aleks.paymygiftmvp.R;
import com.example.aleks.paymygiftmvp.commons.Utils;
import com.example.aleks.paymygiftmvp.model.ResponseCities;
import com.example.aleks.paymygiftmvp.presentation.presenter.main.MainPresenter;
import com.example.aleks.paymygiftmvp.presentation.view.main.MainView;
import com.example.aleks.paymygiftmvp.ui.fragment.login.LoginFragment;
import com.example.aleks.paymygiftmvp.ui.fragment.register.RegisterFragment;
import com.example.aleks.paymygiftmvp.ui.fragment.splash.SplashFragment;
import com.example.aleks.paymygiftmvp.ui.fragment.telephone.TelephoneFragment;
import com.example.aleks.paymygiftmvp.ui.fragment.welcome.WelcomeFragment;
import com.readystatesoftware.systembartint.SystemBarTintManager;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.terrakok.cicerone.Navigator;
import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.android.SupportFragmentNavigator;

public class MainActivity extends MvpAppCompatActivity implements MainView {

    public static final String TAG = "MainActivity";

    @InjectPresenter
    MainPresenter mMainPresenter;
    @BindView(R.id.contentLayout)
    FrameLayout frameLayout;
    @Inject
    NavigatorHolder navigatorHolder;

    private Navigator navigator = new SupportFragmentNavigator(getSupportFragmentManager(), R.id.contentLayout) {
        @Override
        protected Fragment createFragment(String screenKey, Object data) {
            switch (screenKey) {
                case Screens.SPLASH_SCREEN: {
                    return SplashFragment.newInstance();
                }
                case Screens.WELCOME_SCREEN: {
                    return WelcomeFragment.newInstance();
                }
                case Screens.LOGIN_SCREEN: {
                    return LoginFragment.newInstance();
                }
                case Screens.REGISTER_SCREEN: {
                    return RegisterFragment.newInstance((ResponseCities) data);
                }
                case Screens.TELEPHONE_SCREEN: {
                    return TelephoneFragment.newInstance();
                }
                default:
                    throw new RuntimeException("Unknown screen key!");
            }
        }

        @Override
        protected void showSystemMessage(String message) {
            Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
        }

        @Override
        protected void exit() {
            finish();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        PMGApplication.getComponent().inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        frameLayout.setPadding(0, Utils.getStatusBarHeight(this), 0, 0);
        SystemBarTintManager tintManager = new SystemBarTintManager(this);
        tintManager.setStatusBarTintEnabled(true);
        tintManager.setNavigationBarTintEnabled(true);
        tintManager.setTintColor(Color.parseColor("#20000000"));
    }

    @Override
    protected void onResume() {
        super.onResume();
        navigatorHolder.setNavigator(navigator);
    }

    @Override
    protected void onPause() {
        super.onPause();
        navigatorHolder.removeNavigator();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        for (Fragment fragment : fragments) {
            if (fragment instanceof LoginFragment) {
                LoginFragment loginFragment = (LoginFragment) fragment;
                loginFragment.onActivityResult(requestCode, resultCode, data);
            }
        }
    }
}
