package com.example.aleks.paymygiftmvp.presentation.presenter.login;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.example.aleks.paymygiftmvp.PMGApplication;
import com.example.aleks.paymygiftmvp.R;
import com.example.aleks.paymygiftmvp.commons.Utils;
import com.example.aleks.paymygiftmvp.model.FBUser;
import com.example.aleks.paymygiftmvp.model.RegisterUser;
import com.example.aleks.paymygiftmvp.model.ResponseCities;
import com.example.aleks.paymygiftmvp.model.Token;
import com.example.aleks.paymygiftmvp.model.VKUser;
import com.example.aleks.paymygiftmvp.presentation.view.login.LoginView;
import com.example.aleks.paymygiftmvp.service.Service;
import com.example.aleks.paymygiftmvp.ui.activity.main.Screens;
import com.example.aleks.paymygiftmvp.ui.fragment.login.SocialMode;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.HttpMethod;
import com.facebook.login.LoginResult;
import com.google.gson.Gson;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.models.User;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKRequest.VKRequestListener;
import com.vk.sdk.api.VKResponse;

import org.json.JSONException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Response;
import ru.terrakok.cicerone.Router;

@InjectViewState
public class LoginPresenter extends MvpPresenter<LoginView> {

    private static final String TAG = "LoginPresenter";

    @Inject
    SharedPreferences preferences;
    @Inject
    Router router;
    @Inject
    Service service;
    @Inject
    RegisterUser userModel;

    private SocialMode socialMode = null;
    private LoginResult loginResult;

    public LoginPresenter() {
        PMGApplication.getComponent().inject(this);
    }

    private void getCities() {
        Observable<Response<ResponseCities>> request = service.getCities();
        request.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::handleCitiesResponse, this::handleCitiesError);
    }

    private void handleCitiesError(Throwable throwable) {
        throwable.printStackTrace();
        getViewState().hideProgress();
        getViewState().showToastError(R.string.request_error_no_internet);
    }

    private void handleCitiesResponse(Response<ResponseCities> responseCitiesResponse) {
        Log.d(TAG, "cities response code:".concat(String.valueOf(responseCitiesResponse.code())).concat(" message:").concat(responseCitiesResponse.message()));
        getViewState().hideProgress();
        if (responseCitiesResponse.isSuccessful()) {
            goToRegister(responseCitiesResponse.body());
        } else {
            getViewState().showToastError(R.string.request_error_server_error);
        }
    }

    private void goToRegister(ResponseCities body) {
        router.navigateTo(Screens.REGISTER_SCREEN, body);
    }

    private String convertUserId(String usedId) {
        switch (socialMode) {
            case VKONTAKTE: {
                return "vk1".concat(usedId);
            }
            case FACEBOOK: {
                return "fb1".concat(usedId);
            }
            case TWITTER: {
                return "tw1".concat(usedId);
            }
            default:
                throw new RuntimeException("Unsupported Auth Type");
        }
    }

    public void authUser(String userId, SocialMode socialMode) {
        this.socialMode = socialMode;
        getViewState().showProgress();
        Map<String, String> map = new HashMap<>();
        map.put("grant_type", "password");
        map.put("username", convertUserId(userId));
        map.put("password", "");
        Observable<Response<Token>> request = service.authUser(map);
        request
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onAuthResponse, this::onAuthError);
    }

    private void onAuthResponse(Response<Token> tokenResponse) {
        //TODO restore auth process
        loadSocialData();
//        Log.d(TAG,"auth response code:".concat(String.valueOf(tokenResponse.code())).concat(" message:").concat(tokenResponse.message()));
//        if (tokenResponse.isSuccessful()) {
//            //TODO save token to module
//            //TODO save userid to SharedPreferences
//            loadProfileData();
//        } else if (tokenResponse.code() == 400) {
//            loadSocialData();
//        } else {
//            getViewState().hideProgress();
//            getViewState().showToastError(R.string.request_error_server_error);
//        }
    }

    private void onAuthError(Throwable throwable) {
        throwable.printStackTrace();
        getViewState().hideProgress();
        getViewState().showToastError(R.string.request_error_no_internet);
    }

    private void loadSocialData() {
        userModel.clearValues();
        switch (socialMode) {
            case VKONTAKTE: {
                getVKUser();
                break;
            }
            case FACEBOOK: {
                getFBUser();
                break;
            }
            case TWITTER: {
                getTWUser();
                break;
            }
            default:
                throw new RuntimeException("Unsupported Auth Type");
        }
    }

    private void getVKUser() {
        VKParameters parameters = new VKParameters();
        parameters.put(VKApiConst.FIELDS, "id,bdate,sex,photo_400_orig,first_name,last_name");
        VKRequest request = new VKRequest("users.get", parameters);
        request.executeWithListener(new VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                try {
                    VKUser user = new Gson().fromJson(response.json.getJSONArray("response").get(0).toString(), VKUser.class);
                    userModel.setSocialId("vk".concat(String.valueOf(user.getId())));
                    userModel.setNickName(String.format("%s %s", user.getFirstName(), user.getLastName()));
                    if (user.getSex() == 2)
                        userModel.setGender(false);
                    else if (user.getSex() == 1)
                        userModel.setGender(true);
                    SimpleDateFormat simpleDateFormatWithYear = new SimpleDateFormat("dd.MM.yyyy");
                    try {
                        if (user.getBdate() != null) {
                            Date bDate = simpleDateFormatWithYear.parse(user.getBdate());
                            userModel.setDate(Utils.convertFromDateYYYYMMDD(bDate));
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    userModel.setAvatarURL(user.getPhoto400Orig());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.d(TAG, userModel.toString());
                getCities();
            }

            @Override
            public void onError(VKError error) {
                getViewState().hideProgress();
                getViewState().showToastError(R.string.request_error);
            }
        });
    }

    private void getTWUser() {
        Call<User> user = TwitterCore.getInstance().getApiClient().getAccountService().verifyCredentials(false, false, false);
        user.enqueue(new com.twitter.sdk.android.core.Callback<User>() {
            @Override
            public void success(Result<User> userResult) {
                userModel.setNickName(userResult.data.name);
                userModel.setSocialId("tw".concat(String.valueOf(userResult.data.getId())));
                userModel.setLogin(userResult.data.screenName);
                userModel.setAvatarURL(userResult.data.profileImageUrl.replace("_normal", ""));
                Log.d(TAG, userModel.toString());
                getCities();
            }

            @Override
            public void failure(TwitterException exc) {
                exc.printStackTrace();
                getViewState().hideProgress();
                getViewState().showToastError(R.string.request_error);
            }
        });

    }

    private void getFBUser() {
        GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(),
                (object, response) -> {
                    FBUser user = new Gson().fromJson(object.toString(), FBUser.class);
                    Log.d(TAG, object.toString());
                    userModel.setSocialId("fb".concat(user.getId()));
                    userModel.setNickName(user.getName());
                    switch (user.getGender()) {
                        case "male":
                            userModel.setGender(false);
                            break;
                        case "female":
                            userModel.setGender(true);
                            break;
                        default:
                            userModel.setGender(false);
                            break;
                    }
                    SimpleDateFormat simpleDateFormatWithYear = new SimpleDateFormat("MM/dd/yyyy");
                    try {
                        if (user.getBirthday() != null) {
                            Date bDate = simpleDateFormatWithYear.parse(user.getBirthday());
                            userModel.setDate(Utils.convertFromDateYYYYMMDD(bDate));
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    Bundle params = new Bundle();
                    params.putBoolean("redirect", false);
                    params.putString("type", "large");
                    params.putString("height", "400");
                    params.putString("width", "400");
                    new GraphRequest(
                            AccessToken.getCurrentAccessToken(),
                            "/" + "me" + "/picture", params,
                            HttpMethod.GET,
                            response1 -> {
                                try {
                                    userModel.setAvatarURL((String) response1.getJSONObject().getJSONObject("data").get("url"));
                                    Log.d(TAG, userModel.toString());
                                    getCities();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }).executeAsync();
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,gender,birthday,name");
        request.setParameters(parameters);
        request.executeAsync();
    }

    private void loadProfileData() {

    }

    public void setLoginResult(LoginResult loginResult) {
        this.loginResult = loginResult;
    }
}
