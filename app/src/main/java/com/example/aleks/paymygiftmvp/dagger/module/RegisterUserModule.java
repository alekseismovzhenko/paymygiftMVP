package com.example.aleks.paymygiftmvp.dagger.module;

import com.example.aleks.paymygiftmvp.model.RegisterUser;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by aleks on 27.06.2017.
 */
@Module
public class RegisterUserModule {
    private RegisterUser user;

    public RegisterUserModule() {
        user = new RegisterUser();
    }

    @Provides
    @Singleton
    RegisterUser provideRegisterUser() {
        return user;
    }
}
