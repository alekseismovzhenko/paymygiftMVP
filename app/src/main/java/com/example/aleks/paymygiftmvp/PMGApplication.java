package com.example.aleks.paymygiftmvp;

import android.app.Application;

import com.example.aleks.paymygiftmvp.dagger.AppComponent;
import com.example.aleks.paymygiftmvp.dagger.DaggerAppComponent;
import com.example.aleks.paymygiftmvp.dagger.module.NavigatorModule;
import com.example.aleks.paymygiftmvp.dagger.module.RegisterUserModule;
import com.example.aleks.paymygiftmvp.dagger.module.ServiceModule;
import com.example.aleks.paymygiftmvp.dagger.module.SharedPreferencesModule;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.twitter.sdk.android.core.Twitter;
import com.vk.sdk.VKSdk;

/**
 * Created by aleks on 24.06.2017.
 */

public class PMGApplication extends Application {
    private static AppComponent component;

    public static AppComponent getComponent() {
        return component;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        component = buildComponent();
        VKSdk.initialize(this);
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        Twitter.initialize(this);
    }

    protected AppComponent buildComponent() {
        return DaggerAppComponent.builder()
                .sharedPreferencesModule(new SharedPreferencesModule(this))
                .navigatorModule(new NavigatorModule())
                .serviceModule(new ServiceModule())
                .registerUserModule(new RegisterUserModule())
                .build();
        //return null;
    }
}
