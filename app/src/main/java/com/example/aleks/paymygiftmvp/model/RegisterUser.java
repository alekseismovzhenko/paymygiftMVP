package com.example.aleks.paymygiftmvp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Created by aleks on 27.06.2017.
 */

public class RegisterUser {
    @SerializedName("activationId")
    @Expose
    private String activationId;
    @SerializedName("avatarURL")
    @Expose
    private String avatarURL;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("gender")
    @Expose
    private boolean gender;
    @SerializedName("login")
    @Expose
    private String login;
    @SerializedName("mobilePhone")
    @Expose
    private String mobilePhone;
    @SerializedName("nickName")
    @Expose
    private String nickName;
    @SerializedName("smallAvatarURL")
    @Expose
    private String smallAvatarURL;
    @SerializedName("sms")
    @Expose
    private String sms;
    @SerializedName("socialId")
    @Expose
    private String socialId;

    public String getActivationId() {
        return activationId;
    }

    public void setActivationId(String activationId) {
        this.activationId = activationId;
    }

    public String getAvatarURL() {
        return avatarURL;
    }

    public void setAvatarURL(String avatarURL) {
        this.avatarURL = avatarURL;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public boolean isGender() {
        return gender;
    }

    public void setGender(boolean gender) {
        this.gender = gender;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getSmallAvatarURL() {
        return smallAvatarURL;
    }

    public void setSmallAvatarURL(String smallAvatarURL) {
        this.smallAvatarURL = smallAvatarURL;
    }

    public String getSms() {
        return sms;
    }

    public void setSms(String sms) {
        this.sms = sms;
    }

    public String getSocialId() {
        return socialId;
    }

    public void setSocialId(String socialId) {
        this.socialId = socialId;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    public void clearValues() {
        activationId = null;
        avatarURL = null;
        city = null;
        date = null;
        gender = false;
        login = null;
        mobilePhone = null;
        nickName = null;
        smallAvatarURL = null;
        sms = null;
        socialId = null;
    }
}
