package com.example.aleks.paymygiftmvp.presentation.presenter.welcome;


import android.content.SharedPreferences;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.example.aleks.paymygiftmvp.PMGApplication;
import com.example.aleks.paymygiftmvp.commons.Constants;
import com.example.aleks.paymygiftmvp.presentation.view.welcome.WelcomeView;
import com.example.aleks.paymygiftmvp.ui.activity.main.Screens;

import javax.inject.Inject;

import ru.terrakok.cicerone.Router;

@InjectViewState
public class WelcomePresenter extends MvpPresenter<WelcomeView> {

    private static final String TAG = "WelcomePresenter";

    @Inject
    SharedPreferences preferences;
    @Inject
    Router router;

    public WelcomePresenter() {
        PMGApplication.getComponent().inject(this);
    }

    public void goToLogin() {
        preferences.edit()
                .putBoolean(Constants.SHARED_PREFERENCES_IS_LAUNCHED, true)
                .apply();
        router.newRootScreen(Screens.LOGIN_SCREEN);
    }
}
