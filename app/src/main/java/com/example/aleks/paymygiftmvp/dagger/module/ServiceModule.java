package com.example.aleks.paymygiftmvp.dagger.module;

import com.example.aleks.paymygiftmvp.commons.Constants;
import com.example.aleks.paymygiftmvp.service.Service;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by aleks on 25.06.2017.
 */
@Module
public class ServiceModule {
    private Retrofit retrofit;
    private Service service;

    public ServiceModule() {
        this.retrofit = buildRetrofit(buildClient());
        service = createService();
    }

    private OkHttpClient buildClient() {
        return new OkHttpClient.Builder().build();
    }

    private Retrofit buildRetrofit(OkHttpClient client) {
        Retrofit.Builder builder =
                new Retrofit.Builder()
                        .baseUrl(Constants.BASE_URL)
                        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                        .addConverterFactory(
                                GsonConverterFactory.create()
                        );
        return builder
                .client(client)
                .build();
    }

    private Service createService() {
        return retrofit.create(Service.class);
    }

    @Provides
    @Singleton
    Service provideService() {
        return service;
    }
}
