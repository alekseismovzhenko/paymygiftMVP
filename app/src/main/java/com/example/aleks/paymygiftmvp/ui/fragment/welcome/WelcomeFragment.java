package com.example.aleks.paymygiftmvp.ui.fragment.welcome;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.example.aleks.paymygiftmvp.R;
import com.example.aleks.paymygiftmvp.presentation.presenter.welcome.WelcomePresenter;
import com.example.aleks.paymygiftmvp.presentation.view.welcome.WelcomeView;
import com.synnapps.carouselview.CarouselView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class WelcomeFragment extends MvpAppCompatFragment implements WelcomeView {

    public static final String TAG = "WelcomeFragment";
    private final static int NUM_OF_PAGES = 3;

    @InjectPresenter
    WelcomePresenter mWelcomePresenter;
    @BindView(R.id.welcomeCarouselView)
    CarouselView carouselView;


    public static WelcomeFragment newInstance() {
        WelcomeFragment fragment = new WelcomeFragment();

        Bundle args = new Bundle();
        fragment.setArguments(args);

        return fragment;
    }

    @SuppressLint("InflateParams")
    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             final Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_welcome, container, false);
        ButterKnife.bind(this, view);
        carouselView.setPageCount(NUM_OF_PAGES);
        carouselView.setViewListener(position -> inflater.inflate(R.layout.layout_welcome_page, null));
        return view;
    }


    @OnClick(R.id.welcomeStartButton)
    public void goToLogin() {
        mWelcomePresenter.goToLogin();
    }
}
