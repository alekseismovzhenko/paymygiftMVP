package com.example.aleks.paymygiftmvp.ui.activity.main;

/**
 * Created by aleks on 24.06.2017.
 */

public interface Screens {
    String SPLASH_SCREEN = "splash";
    String WELCOME_SCREEN = "welcome";
    String LOGIN_SCREEN = "login";
    String REGISTER_SCREEN = "register";
    String TELEPHONE_SCREEN = "telephone";
}
