package com.example.aleks.paymygiftmvp.presentation.presenter.splash;


import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.example.aleks.paymygiftmvp.PMGApplication;
import com.example.aleks.paymygiftmvp.presentation.view.splash.SplashView;

@InjectViewState
public class SplashPresenter extends MvpPresenter<SplashView> {

    public SplashPresenter() {
        PMGApplication.getComponent().inject(this);
    }
}
