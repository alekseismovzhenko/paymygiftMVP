package com.example.aleks.paymygiftmvp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.List;

/**
 * Created by aleks on 25.06.2017.
 */

public class ResponseCities implements Serializable {
    @SerializedName("citys")
    @Expose
    private List<String> citys = null;

    public List<String> getCitys() {
        return citys;
    }

    public void setCitys(List<String> citys) {
        this.citys = citys;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
