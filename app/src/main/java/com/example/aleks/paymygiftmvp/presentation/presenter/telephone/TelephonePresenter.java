package com.example.aleks.paymygiftmvp.presentation.presenter.telephone;


import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.example.aleks.paymygiftmvp.PMGApplication;
import com.example.aleks.paymygiftmvp.model.RegisterUser;
import com.example.aleks.paymygiftmvp.presentation.view.telephone.TelephoneView;

import javax.inject.Inject;

import ru.terrakok.cicerone.Router;

@InjectViewState
public class TelephonePresenter extends MvpPresenter<TelephoneView> {
    private static final String TAG = "TelephonePresenter";

    @Inject
    Router router;
    @Inject
    RegisterUser userModel;

    public TelephonePresenter() {
        PMGApplication.getComponent().inject(this);
        bindValues();
    }

    private void bindValues() {

    }
}
