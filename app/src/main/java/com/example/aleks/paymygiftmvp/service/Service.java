package com.example.aleks.paymygiftmvp.service;

import com.example.aleks.paymygiftmvp.model.ResponseCities;
import com.example.aleks.paymygiftmvp.model.Token;

import java.util.Map;

import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;

/**
 * Created by aleks on 25.06.2017.
 */

public interface Service {
    @GET("city/default")
    Observable<Response<ResponseCities>> getCities();

    @Headers("Authorization: Basic NGltU09sdE1EcnFaTW80S0VrNVJEODJUMFI3VkUyOWZaVjA0ejhYeGwwUm9SOWlXeUp1cDZRS016U2l2M20zQTdwM0Y3OnJyVmpqbk4weUZnbDhQMlJ2WjdqUnR5OFJuNjlnTVpsWHlFNGVhSVNQbjBZOXpXaVBVNGxBdzc2Y1hxNGdEMmc0dlpjNDc=")
    @POST("oauth/token")
    Observable<Response<Token>> authUser(@QueryMap Map<String, String> params);
}
