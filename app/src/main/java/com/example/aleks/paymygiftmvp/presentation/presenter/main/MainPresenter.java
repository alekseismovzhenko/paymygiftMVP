package com.example.aleks.paymygiftmvp.presentation.presenter.main;


import android.content.SharedPreferences;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.example.aleks.paymygiftmvp.PMGApplication;
import com.example.aleks.paymygiftmvp.commons.Constants;
import com.example.aleks.paymygiftmvp.presentation.view.main.MainView;
import com.example.aleks.paymygiftmvp.ui.activity.main.Screens;

import javax.inject.Inject;

import ru.terrakok.cicerone.Router;

@InjectViewState
public class MainPresenter extends MvpPresenter<MainView> {

    private static final String TAG = "MainPresenter";

    @Inject
    SharedPreferences preferences;
    @Inject
    Router router;


    public MainPresenter() {
        PMGApplication.getComponent().inject(this);
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        loadInitialFragment();
    }

    private void loadInitialFragment() {
        boolean isLaunched = preferences.getBoolean(Constants.SHARED_PREFERENCES_IS_LAUNCHED, false);
        if (!isLaunched) {
            router.newRootScreen(Screens.WELCOME_SCREEN);
        } else {
            String userId = preferences.getString(Constants.SHARED_PREFERENCES_USER_ID, null);
            if (userId == null) {
                router.newRootScreen(Screens.LOGIN_SCREEN);
            } else {
                router.newRootScreen(Screens.SPLASH_SCREEN);
            }
        }
    }
}
