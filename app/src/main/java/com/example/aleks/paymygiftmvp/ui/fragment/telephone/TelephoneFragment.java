package com.example.aleks.paymygiftmvp.ui.fragment.telephone;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.example.aleks.paymygiftmvp.R;
import com.example.aleks.paymygiftmvp.presentation.presenter.telephone.TelephonePresenter;
import com.example.aleks.paymygiftmvp.presentation.view.telephone.TelephoneView;

public class TelephoneFragment extends MvpAppCompatFragment implements TelephoneView {
    public static final String TAG = "TelephoneFragment";
    @InjectPresenter
    TelephonePresenter mTelephonePresenter;

    public static TelephoneFragment newInstance() {
        TelephoneFragment fragment = new TelephoneFragment();

        Bundle args = new Bundle();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             final Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_telephone, container, false);
        return view;
    }

}
