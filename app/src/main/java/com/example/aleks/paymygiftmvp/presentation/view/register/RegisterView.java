package com.example.aleks.paymygiftmvp.presentation.view.register;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

public interface RegisterView extends MvpView {
    @StateStrategyType(AddToEndSingleStrategy.class)
    void setAvatar(Object object);

    @StateStrategyType(AddToEndSingleStrategy.class)
    void setName(String string);

    @StateStrategyType(AddToEndSingleStrategy.class)
    void setNickName(String string);

    @StateStrategyType(AddToEndSingleStrategy.class)
    void setCity(String string);

    @StateStrategyType(AddToEndSingleStrategy.class)
    void setGender(boolean gender);

    @StateStrategyType(AddToEndSingleStrategy.class)
    void setBirthday(String string);

    @StateStrategyType(AddToEndSingleStrategy.class)
    void setBirthdayError(Integer error);

    @StateStrategyType(AddToEndSingleStrategy.class)
    void setNameError(Integer error);

    @StateStrategyType(AddToEndSingleStrategy.class)
    void setNickNameError(Integer error);

    @StateStrategyType(AddToEndSingleStrategy.class)
    void setCityError(Integer error);
}
