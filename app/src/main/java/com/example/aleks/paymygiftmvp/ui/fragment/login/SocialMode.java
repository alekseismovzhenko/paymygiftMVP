package com.example.aleks.paymygiftmvp.ui.fragment.login;

/**
 * Created by aleks on 25.06.2017.
 */

public enum SocialMode {
    VKONTAKTE,
    FACEBOOK,
    TWITTER
}
