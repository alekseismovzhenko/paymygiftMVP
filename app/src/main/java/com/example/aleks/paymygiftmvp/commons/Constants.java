package com.example.aleks.paymygiftmvp.commons;

/**
 * Created by aleks on 24.06.2017.
 */

public interface Constants {
    String SHARED_PREFERENCES = "shared_preferences";
    String SHARED_PREFERENCES_IS_LAUNCHED = "is_launched";
    String SHARED_PREFERENCES_USER_ID = "user_id";
    String BASE_URL = "http://45.55.219.163/";
}
