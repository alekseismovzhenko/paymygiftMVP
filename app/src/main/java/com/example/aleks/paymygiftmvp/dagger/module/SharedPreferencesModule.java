package com.example.aleks.paymygiftmvp.dagger.module;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.aleks.paymygiftmvp.commons.Constants;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by aleks on 24.06.2017.
 */
@Module
public class SharedPreferencesModule {
    private SharedPreferences preferences;

    public SharedPreferencesModule(Context context) {
        this.preferences = context.getSharedPreferences(Constants.SHARED_PREFERENCES, Context.MODE_PRIVATE);
    }

    @Provides
    @Singleton
    SharedPreferences provideSharedPreferences() {
        return preferences;
    }
}
