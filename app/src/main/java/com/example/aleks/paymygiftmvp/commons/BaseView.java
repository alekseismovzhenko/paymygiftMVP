package com.example.aleks.paymygiftmvp.commons;

import com.arellomobile.mvp.MvpView;

/**
 * Created by aleks on 25.06.2017.
 */

public interface BaseView extends MvpView {
    void showProgress();

    void hideProgress();

    void showToastError(int resId);
}
