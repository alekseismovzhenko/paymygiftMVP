package com.example.aleks.paymygiftmvp.dagger;

import com.example.aleks.paymygiftmvp.dagger.module.NavigatorModule;
import com.example.aleks.paymygiftmvp.dagger.module.RegisterUserModule;
import com.example.aleks.paymygiftmvp.dagger.module.ServiceModule;
import com.example.aleks.paymygiftmvp.dagger.module.SharedPreferencesModule;
import com.example.aleks.paymygiftmvp.presentation.presenter.login.LoginPresenter;
import com.example.aleks.paymygiftmvp.presentation.presenter.main.MainPresenter;
import com.example.aleks.paymygiftmvp.presentation.presenter.register.RegisterPresenter;
import com.example.aleks.paymygiftmvp.presentation.presenter.splash.SplashPresenter;
import com.example.aleks.paymygiftmvp.presentation.presenter.telephone.TelephonePresenter;
import com.example.aleks.paymygiftmvp.presentation.presenter.welcome.WelcomePresenter;
import com.example.aleks.paymygiftmvp.ui.activity.main.MainActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by aleks on 24.06.2017.
 */
@Singleton
@Component(modules = {
        SharedPreferencesModule.class,
        NavigatorModule.class,
        ServiceModule.class,
        RegisterUserModule.class})
public interface AppComponent {
    void inject(MainPresenter presenter);

    void inject(MainActivity activity);

    void inject(WelcomePresenter presenter);

    void inject(SplashPresenter presenter);

    void inject(LoginPresenter presenter);

    void inject(RegisterPresenter presenter);

    void inject(TelephonePresenter presenter);
}
